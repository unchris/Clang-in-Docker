/**
  Attempting something like Reversi maybe
  To compile: clang++ -std=c++17 -stdlib=libc++ -Wall -pedantic test_fs.cpp -o test_fs -lc++experimental
  Using the docker container from https://solarianprogrammer.com/2017/12/14/clang-in-docker-container-cpp-17-development/
  Found at https://github.com/sol-prog/Clang-in-Docker.git
  And a quick refresher course on C++11 found here https://www.youtube.com/watch?v=U6mgsPqV32A

  Author: Chris Cameron
*/

#include <iostream>
#include <experimental/filesystem>

#include <initializer_list>
#include <vector>
#include <string>

class gameBoard {
  std::vector<char> * board;
  char last_piece = '_';

  const int ROWMAX = 0;
  const int COLMAX = 7;

  int black_left = 32;
  int white_left = 32;

  bool game_over = false;

public:

  gameBoard(const std::initializer_list<char>& v) {
    this->board = new std::vector<char>();
    for (auto itr = v.begin(); itr!= v.end(); ++itr) {
      board->push_back(*itr);
    }
  }

  void print_board() {
    for (auto itr = this->board->begin(); itr != this->board->end(); ++itr) {
      std::cout << *itr << " ";
    }
    std::cout << '\n';
  }

  bool validate(int row, int col, char piece) {
    if (this->game_over == true) {
      return false;
    }

    if (this->last_piece == piece) {
      return false;
    }

    if (row > this->ROWMAX || row < 0 || col > this->COLMAX || col < 0) {
      return false;
    }

    if (this->board[row][col] != '_') {
      return false;
    }

    if (piece != 'W' && piece != 'B') {
      return false;
    }

    // TBD: other rules

    return true;
  }

  void decrement(char piece) {
    if (piece == 'B') {
      this->black_left--;
    } else {
      this->white_left--;
    }
  }

  void set_game_over() {
    if (white_left == 0 || black_left == 0) {
      this->game_over = true;
      std::cout << "game over" << "\n";
    }

    //TBD: other rules
  }

  void set(int row, int col, char piece) {
    std::cout << "Move: (" << row << "," << col << "): " << piece << "\n";
    if (!validate(row, col, piece)) {
      std::cout << "invalid move" << "\n";
      return;
    }

    decrement(piece);
    board[row][col] = piece;
    this->last_piece = piece;

    set_game_over();
  }


};

int main() {
  for (auto &file : std::experimental::filesystem::recursive_directory_iterator("./")) {
    std::cout << file << "\n";
  }

  auto test_board = new gameBoard({'_', '_', '_', '_', '_', '_', '_', '_'});

  test_board->print_board();

  test_board->set(0, 4, 'B');
  test_board->print_board();

  test_board->set(0, 4, 'B');
  test_board->print_board();

  test_board->set(0, 4, 'W');
  test_board->print_board();

  test_board->set(0, 0, 'A');
  test_board->print_board();
  return 0;
}
